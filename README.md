Read-only DB connection
-----------------------

This allows to configure a read-only DB connection for additional security on
websites only accessible to anonymous users.

This relies on a set of yet unreleased core patches and set of advanced settings
allowing to configure alternative storages for cache bins and logging. The
assumption is that an alternative read-write DB connection is configured in a
separate site instance not accessible from the internet to allow for site
maintenance.
