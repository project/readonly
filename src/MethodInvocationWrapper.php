<?php

namespace Drupal\readonly;

use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Site\Settings;

/**
 * Trait useful to wrap methods performing DB write queries.
 */
trait MethodInvocationWrapper {

  /**
   * A local cache of suppressed error messages.
   *
   * @var string[]
   */
  protected static ?array $suppressedErrors;

  /**
   * Whether suppressed errors should be logged.
   *
   * @var bool
   */
  protected static bool $loggingEnabled;

  /**
   * Wraps method invocations catching exceptions on write.
   *
   * @param callable $method_invocation
   *   A method callback.
   *
   * @return mixed|null
   *   The method return value or NULL if an exception was thrown.
   */
  protected function wrapMethodInvocation(callable $method_invocation) {
    try {
      return $method_invocation();
    }
    catch (DatabaseExceptionWrapper $e) {
      // Initialize static state.
      if (!isset(static::$suppressedErrors)) {
        static::$suppressedErrors = Settings::get('readonly.suppressed_errors') ?: [
          'SQLSTATE[HY000]: General error: 1290',
          'SQLSTATE[42000]: Syntax error or access violation: 1142',
        ];

        static::$loggingEnabled = (bool) Settings::get('readonly.logging_enabled');
      }

      // Catch only write access issues.
      $message = $e->getMessage();
      foreach (static::$suppressedErrors as $error) {
        if (str_starts_with($message, $error)) {
          if (static::$loggingEnabled) {
            watchdog_exception('readonly', $e);
          }
          return NULL;
        }
      }

      throw $e;
    }
  }

}
