<?php

namespace Drupal\readonly\Services;

use Drupal\readonly\MethodInvocationWrapper;
use Drupal\Core\KeyValueStore\DatabaseStorage;

/**
 * Read-Only expirable Key-Value database storage.
 */
class ReadOnlyDatabaseStorage extends DatabaseStorage {

  use MethodInvocationWrapper;

  /**
   * {@inheritdoc}
   */
  protected function doSet($key, $value) {
    $this->wrapMethodInvocation(function () use ($key, $value) {
      parent::doSet($key, $value);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function doSetIfNotExists($key, $value) {
    return $this->wrapMethodInvocation(function () use ($key, $value) {
      return parent::doSetIfNotExists($key, $value);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function rename($key, $new_key) {
    $this->wrapMethodInvocation(function () use ($key, $new_key) {
      parent::rename($key, $new_key);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(array $keys) {
    $this->wrapMethodInvocation(function () use ($keys) {
      parent::deleteMultiple($keys);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll() {
    $this->wrapMethodInvocation(function () {
      parent::deleteAll();
    });
  }


}
