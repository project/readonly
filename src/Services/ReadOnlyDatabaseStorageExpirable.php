<?php

namespace Drupal\readonly\Services;

use Drupal\readonly\MethodInvocationWrapper;
use Drupal\Core\KeyValueStore\DatabaseStorageExpirable;

/**
 * Read-Only expirable Key-Value database storage.
 */
class ReadOnlyDatabaseStorageExpirable extends DatabaseStorageExpirable {

  use MethodInvocationWrapper;

  /**
   * {@inheritdoc}
   */
  protected function doSetWithExpire($key, $value, $expire) {
    $this->wrapMethodInvocation(function () use ($key, $value, $expire) {
      parent::doSetWithExpire($key, $value, $expire);
    });
  }

}
