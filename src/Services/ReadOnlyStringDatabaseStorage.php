<?php

namespace Drupal\readonly\Services;

use Drupal\readonly\MethodInvocationWrapper;
use Drupal\locale\StringInterface;
use Drupal\locale\StringStorageInterface;

/**
 * Read-only string database storage.
 */
final class ReadOnlyStringDatabaseStorage implements StringStorageInterface {

  use MethodInvocationWrapper;

  /**
   * The decorated storage.
   *
   * @var \Drupal\locale\StringStorageInterface
   */
  protected StringStorageInterface $storage;

  /**
   * StringDatabaseStorage
   */
  public function __construct(StringStorageInterface $storage) {
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public function getStrings(array $conditions = [], array $options = []) {
    $strings = $this->storage->getStrings($conditions, $options);
    return $this->setStringsStorage($strings);
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslations(array $conditions = [], array $options = []) {
    $translations = $this->storage->getTranslations($conditions, $options);
    return $this->setStringsStorage($translations);
  }

  /**
   * {@inheritdoc}
   */
  public function getLocations(array $conditions = []) {
    return $this->storage->getLocations($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function findString(array $conditions) {
    $string = $this->storage->findString($conditions);
    if ($string) {
      $string->setStorage($this);
    }
    return $string;
  }

  /**
   * {@inheritdoc}
   */
  public function findTranslation(array $conditions) {
    $translation = $this->storage->findTranslation($conditions);
    if ($translation) {
      $translation->setStorage($this);
    }
    return $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function save($string) {
    return $this->wrapMethodInvocation(function () use ($string) {
      return $this->storage->save($string);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function delete($string) {
    return $this->wrapMethodInvocation(function () use ($string) {
      return $this->storage->delete($string);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function deleteStrings($conditions) {
    $this->wrapMethodInvocation(function () use ($conditions) {
      $this->storage->deleteStrings($conditions);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTranslations($conditions) {
    $this->wrapMethodInvocation(function () use ($conditions) {
      $this->storage->deleteTranslations($conditions);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function countStrings() {
    return $this->storage->countStrings();
  }

  /**
   * {@inheritdoc}
   */
  public function countTranslations() {
    return $this->storage->countTranslations();
  }

  /**
   * {@inheritdoc}
   */
  public function createString($values = []) {
    return $this->wrapMethodInvocation(function () use ($values) {
      $string = $this->storage->createString($values);
      if ($string) {
        $string->setStorage($this);
      }
      return $string;
    });
  }

  /**
   * {@inheritdoc}
   */
  public function createTranslation($values = []) {
    return $this->wrapMethodInvocation(function () use ($values) {
      $translation = $this->storage->createTranslation($values);
      if ($translation) {
        $translation->setStorage($this);
      }
      return $translation;
    });
  }

  /**
   * Sets this as the string storage.
   *
   * @param \Drupal\locale\StringInterface[] $strings
   *   An array of strings.
   *
   * @return \Drupal\locale\StringInterface[]
   *   The processed strings.
   */
  protected function setStringsStorage(array $strings): array {
    return array_map(
      function (StringInterface $string) {
        $string->setStorage($this);
        return $string;
      },
      $strings
    );
  }

}
