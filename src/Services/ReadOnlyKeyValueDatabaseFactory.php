<?php

namespace Drupal\readonly\Services;

use Drupal\Core\KeyValueStore\KeyValueDatabaseFactory;

/**
 * Read-only Key-Value database factory.
 */
class ReadOnlyKeyValueDatabaseFactory extends KeyValueDatabaseFactory {

  /**
   * {@inheritdoc}
   */
  public function get($collection) {
    return new ReadOnlyDatabaseStorage($collection, $this->serializer, $this->connection);
  }

}
