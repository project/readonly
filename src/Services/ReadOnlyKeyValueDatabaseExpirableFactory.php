<?php

namespace Drupal\readonly\Services;

use Drupal\Core\KeyValueStore\KeyValueDatabaseExpirableFactory;

/**
 * Read-only expirable Key-Value database factory.
 */
class ReadOnlyKeyValueDatabaseExpirableFactory extends KeyValueDatabaseExpirableFactory {

  /**
   * {@inheritdoc}
   */
  public function get($collection) {
    if (!isset($this->storages[$collection])) {
      $this->storages[$collection] = new ReadOnlyDatabaseStorageExpirable($collection, $this->serializer, $this->connection);
    }
    return $this->storages[$collection];
  }

}
