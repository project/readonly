<?php

namespace Drupal\readonly\Services;

use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\readonly\MethodInvocationWrapper;
use Psr\Log\LoggerInterface;

/**
 * Read-only database logger.
 */
class ReadonlyLogger implements LoggerInterface {

  use RfcLoggerTrait;
  use MethodInvocationWrapper;

  /**
   * The decorated DB logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * ReadonlyLogger constructor.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;

    // If an error is triggered while logging, we do not want to trigger further
    // errors.
    static::$loggingEnabled = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    $this->wrapMethodInvocation(function () use ($level, $message, $context) {
      $this->logger->log($level, $message, $context);
    });
  }

}
