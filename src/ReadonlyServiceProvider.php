<?php

namespace Drupal\readonly;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\readonly\Services\ReadOnlyKeyValueDatabaseExpirableFactory;
use Drupal\readonly\Services\ReadOnlyKeyValueDatabaseFactory;
use Drupal\readonly\Services\ReadonlyLogger;
use Drupal\readonly\Services\ReadOnlyStringDatabaseStorage;
use Symfony\Component\DependencyInjection\Reference;

/**
 * The Read-Only service provider.
 */
class ReadonlyServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $service_id = 'locale.storage';
    if ($container->has($service_id)) {
      $container->register("readonly.$service_id", ReadOnlyStringDatabaseStorage::class)
        ->setDecoratedService($service_id)
        ->addArgument(new Reference("readonly.$service_id.inner"));
    }

    $service_id = 'logger.dblog';
    if ($container->has($service_id)) {
      $container->register("readonly.$service_id", ReadonlyLogger::class)
        ->setDecoratedService($service_id)
        ->addArgument(new Reference("readonly.$service_id.inner"));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('keyvalue.database')
      ->setClass(ReadOnlyKeyValueDatabaseFactory::class);
    $container->getDefinition('keyvalue.expirable.database')
      ->setClass(ReadOnlyKeyValueDatabaseExpirableFactory::class);
  }

}
